/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package person7;
public class Person {
    private double weight;
    private double height; 
    private String name;
    Weight unitWeight;
    Height unitHeight;
     
    public Person(String name){
    this.name=name;    
    }
    public Person (String name, double weight,Weight unitWeight, double height, Height unitHeight)
    {
    this.name =name;
    this.weight=weight;
    this.height=height;
    }
    public double getWeight(){
    return this.weight;
    }
    public void setWeight(double weight){
    this.weight = weight;
    }
    
    public double getHeight() {
        return height;
    }
    public void setHeight(double height) {
        this.height = height;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public double getBMI(){
    double weightInKilos = 0;
    double heightInMeters = 0;
    switch(unitWeight)
        {
            case K: weightInKilos = weight;
            break;
            case LB: weightInKilos = weight * 0.4535;
            break;
        }
        
        switch(unitHeight)
        {
            case M: heightInMeters = height;
            break;
            case IN: heightInMeters = height * 0.0254;
            break;
        }

    return weightInKilos;
    }
    
    public double getWeightLbs(){
    return this.weight*2.2;    //convert from kg to pounds
    }
    
    public double getHeightInches(){
    return this.height*39.3701; //convert from meter to inches
    }

    @Override
    public String toString() {
        return  "Person Name " + name + "\t" + "BMI =  " + getBMI()  ;
    }
}
